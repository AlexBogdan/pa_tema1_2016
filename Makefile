build:
	g++ points.cpp -o points -std=c++11
	g++ adn.cpp -o adn -std=c++11
	g++ stropitori.cpp -o stropitori -std=c++11
	g++ warriors.cpp -o warriors -std=c++11
run-p1:
	./points
run-p2:
	./adn
run-p3:
	./stropitori
run-p4:
	./warriors
clean:
	rm points adn stropitori warriors

// Copyright 2017 Andrei Bogdan Alexandru

#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <ctime>
#include <cmath>

using namespace std;

ifstream in("adn.in");
ofstream out("adn.out");

/*
	Verificam liniar daca fiecare caracter din A este si in T (Target)
*/
bool verify1(string& A, string& T) {
	int N = A.length();
	if (N  !=  T.length()) {  // Tratam exceptia
		return false;
	}
	for (int i = 0; i < N ; i++) {
		if (A[i]  !=  T[i]) {
			return false;
		}
	}
	return true;
}

/*
	Implementarea algoritmului descris pe GeeksforGeeks (link in README)
*/
bool verify2(string& A, string& B, string& T) {
	int M = A.length(), N = B.length();

	if ((M+N)  !=  T.length())
		return false;

	bool IL[M+1][N+1];

	memset(IL, 0, sizeof(IL));

	for (int i=0; i  <=  M; ++i) {
					for (int j=0; j  <=  N; ++j) {
									if (i == 0 && j == 0)
													IL[i][j] = true;

									else if (i == 0 && B[j-1] == T[j-1])
													IL[i][j] = IL[i][j-1];

									else if (j == 0 && A[i-1] == T[i-1])
													IL[i][j] = IL[i-1][j];

									else if (A[i-1] == T[i+j-1] && B[j-1] != T[i+j-1])
													IL[i][j] = IL[i-1][j];

									else if (A[i-1] != T[i+j-1] && B[j-1] == T[i+j-1])
													IL[i][j] = IL[i][j-1];

									else if (A[i-1] == T[i+j-1] && B[j-1] == T[i+j-1])
													IL[i][j]=(IL[i-1][j] || IL[i][j-1]);
					}
	}

	return IL[M][N];
}

bool verify3(string& A, string& B, string& C, string& T) {
	int M = A.length(), N = B.length(), L = C.length();

	if ((M+N+L)  !=  T.length())
				return false;

	bool IL[M+1][N+1][L+1];

	memset(IL, 0, sizeof(IL));

	for (int i=0; i <= M; ++i) {
					for (int j=0; j <= N; ++j) {
									for (int k=0 ; k <= L ; ++k) {
													if (i == 0 && j == 0 && k == 0) {
																	IL[i][j][k] = true;
													} else if (i == 0 && j == 0 && C[k-1] == T[k-1]) {
																	IL[i][j][k] = IL[i][j][k-1];
													} else if (i == 0 && B[j-1] == T[j-1] && k == 0) {
																	IL[i][j][k] = IL[i][j-1][k];
													} else if (A[i-1] == T[i-1] && j == 0 && k == 0) {
																	IL[i][j][k] = IL[i-1][j][k];
													} else if (k == 0 || C[k-1]  !=  T[i+j+k-1]) {
																					if (A[i-1] == T[i+j+k-1] && B[j-1] != T[i+j+k-1])
																									IL[i][j][k] = IL[i-1][j][k];

																					else if (A[i-1] != T[i+j+k-1] && B[j-1] == T[i+j+k-1])
																									IL[i][j][k] = IL[i][j-1][k];

																					else if (A[i-1] == T[i+j+k-1] && B[j-1] == T[i+j+k-1])
																									IL[i][j][k] =(IL[i-1][j][k] || IL[i][j-1][k]);
													} else if (j == 0 || B[j-1]  !=  T[i+j+k-1]) {
																					if (A[i-1] == T[i+j+k-1] && C[k-1] != T[i+j+k-1])
																					IL[i][j][k] = IL[i-1][j][k];

																					else if (A[i-1] != T[i+j+k-1] && C[k-1] == T[i+j+k-1])
																									IL[i][j][k] = IL[i][j][k-1];

																					else if (A[i-1] == T[i+j+k-1] && C[k-1] == T[i+j+k-1])
																									IL[i][j][k] =(IL[i-1][j][k] || IL[i][j][k-1]);
													} else if (i == 0 || A[i-1] !=  T[i+j+k-1]) {
																					if (B[j-1] == T[i+j+k-1] && C[k-1] != T[i+j+k-1])
																					IL[i][j][k] = IL[i][j-1][k];

																					else if (B[j-1] != T[i+j+k-1] && C[k-1] == T[i+j+k-1])
																									IL[i][j][k] = IL[i][j][k-1];

																					else if (B[j-1] == T[i+j+k-1] && C[k-1] == T[i+j+k-1])
																									IL[i][j][k] =(IL[i][j-1][k] || IL[i][j][k-1]);
													} else {
																	IL[i][j][k] =(IL[i-1][j][k] || IL[i][j-1][k] ||
																								IL[i][j][k-1]);
										}
									}
					}
	}

	return IL[M][N][L];
}

bool verify4(string& A, string& B, string& C, string& D, string& T) {
	int M = A.length(), N = B.length(), L = C.length(), O = D.length();
	if ((M+N+L+O)  !=  T.length())
				return false;

	bool IL[M+1][N+1][L+1][O+1];

	memset(IL, 0, sizeof(IL));

	for (int i=0; i <= M; ++i) {
		for (int j=0; j <= N; ++j) {
			for (int k=0 ; k <= L ; ++k)  {
				for (int u=0 ; u <= O ; ++u) {
								if (i == 0 && j == 0 && k == 0 && u == 0) {
												IL[i][j][k][u] = true;
								} else if (i == 0 && j == 0 && k == 0 && D[u-1] == T[u-1]) {
												IL[i][j][k][u] = IL[i][j][k][u-1];
								} else if (i == 0 && j == 0 && C[k-1] == T[k-1] && u == 0) {
												IL[i][j][k][u] = IL[i][j][k-1][u];
								} else if (i == 0 && B[j-1] == T[j-1] && k == 0 && u == 0) {
												IL[i][j][k][u] = IL[i][j-1][k][u];
								} else if (A[i-1] == T[i-1] && j == 0 && k == 0 && u == 0) {
												IL[i][j][k][u] = IL[i-1][j][k][u];
								} else if (u == 0 || D[u-1]  !=  T[i+j+k+u-1]) {
													if (k == 0 || C[k-1]  !=  T[i+j+k+u-1]) {
															if (A[i-1] == T[i+j+k+u-1] && B[j-1] != T[i+j+k+u-1])
																IL[i][j][k][u] = IL[i-1][j][k][u];

													else if (A[i-1] != T[i+j+k+u-1] && B[j-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];

													else if (A[i-1] == T[i+j+k+u-1] && B[j-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j-1][k][u]);
									} else if (j == 0 || B[j-1]  !=  T[i+j+k+u-1]) {
													if (A[i-1] == T[i+j+k+u-1] && C[k-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i-1][j][k][u];
													else if (A[i-1] != T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (A[i-1] == T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j][k-1][u]);
									} else if (i == 0 || A[i-1] !=  T[i+j+k+u-1]) {
													if (B[j-1] == T[i+j+k+u-1] && C[k-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];
													else if (B[j-1] != T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (B[j-1] == T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j-1][k][u] || IL[i][j][k-1][u]);
									} else {
												IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j-1][k][u]
														|| IL[i][j][k-1][u]);
											}
					} else if (k == 0 || C[k-1]  !=  T[i+j+k+u-1]) {
									if (u == 0 || D[u-1]  !=  T[i+j+k+u-1]) {
													if (A[i-1] == T[i+j+k+u-1] && B[j-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i-1][j][k][u];
													else if (A[i-1] != T[i+j+k+u-1] && B[j-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];
													else if (A[i-1] == T[i+j+k+u-1] && B[j-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j-1][k][u]);
									} else if (j == 0 || B[j-1]  !=  T[i+j+k+u-1]) {
													if (A[i-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i-1][j][k][u];
													else if (A[i-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (A[i-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j][k][u-1]);
									} else if (i == 0 || A[i-1] !=  T[i+j+k+u-1]) {
													if (B[j-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];
													else if (B[j-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (B[j-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j-1][k][u] || IL[i][j][k][u-1]);
									} else {
												IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j-1][k][u]
																	|| IL[i][j][k][u-1]);
											}
					} else if (j == 0 || B[j-1]  !=  T[i+j+k+u-1]) {
									if (u == 0 || D[u-1]  !=  T[i+j+k+u-1]) {
													if (A[i-1] == T[i+j+k+u-1] && C[k-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i-1][j][k][u];
													else if (A[i-1] != T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (A[i-1] == T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j][k-1][u]);
									} else if (k == 0 || C[k-1]  !=  T[i+j+k+u-1]) {
													if (A[i-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i-1][j][k][u];
													else if (A[i-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (A[i-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j][k][u-1]);
									} else if (i == 0 || A[i-1] !=  T[i+j+k+u-1]) {
													if (C[k-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (C[k-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (C[k-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j][k-1][u] || IL[i][j][k][u-1]);
									} else {
													IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j][k-1][u]
															|| IL[i][j][k][u-1]);
											}
					} else if (i == 0 || A[i-1]  !=  T[i+j+k+u-1]) {
									if (u == 0 || D[u-1]  !=  T[i+j+k+u-1]) {
													if (B[j-1] == T[i+j+k+u-1] && C[k-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];
													else if (B[j-1] != T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (B[j-1] == T[i+j+k+u-1] && C[k-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j-1][k][u] || IL[i][j][k-1][u]);
									} else if (k == 0 || C[k-1]  !=  T[i+j+k+u-1]) {
													if (B[j-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j-1][k][u];
													else if (B[j-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (B[j-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j-1][k][u] || IL[i][j][k][u-1]);
									} else if (j == 0 || B[j-1] !=  T[i+j+k+u-1]) {
													if (C[k-1] == T[i+j+k+u-1] && D[u-1] != T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k-1][u];
													else if (C[k-1] != T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] = IL[i][j][k][u-1];
													else if (C[k-1] == T[i+j+k+u-1] && D[u-1] == T[i+j+k+u-1])
																	IL[i][j][k][u] =(IL[i][j][k-1][u] || IL[i][j][k][u-1]);
									} else {
													IL[i][j][k][u] =(IL[i][j-1][k][u] || IL[i][j][k-1][u]
															|| IL[i][j][k][u-1]);
										 }
					} else {
								IL[i][j][k][u] =(IL[i-1][j][k][u] || IL[i][j-1][k][u] ||
						IL[i][j][k-1][u] || IL[i][j][k][u-1]);
					}
				}
			}
		}
	}
	return IL[M][N][L][O];
}

void test() {
// -=-=-=-=-=-=-=- Declarari + Input -=-=-=-=-=-=-=-=-=
	int n, i;
	string probe, target;
	vector<string> probes;

	in >> n;
	getline(in, probe, ' ');
	for (i=0; i < n ; i++) {
				in >> probe;
				probes.push_back(probe);
	}
	in >> target;

	switch (n) {
		case 1:
			out << verify1(probes[0], target) << endl;
			break;
		case 2:
			out << verify2(probes[0], probes[1], target) << endl;
			break;
					case 3:
									out << verify3(probes[0], probes[1], probes[2], target) << endl;
									break;
					case 4:
									out << verify4(probes[0], probes[1], probes[2], probes[3], target);
									out << endl;
									break;
	}

	return;
}

int main() {
	int m;
	in >> m;

	for (int i=1; i <= m; i++) {
					test();
	}  // Verificam cele m linii

	in.close();
	out.close();

	return 0;
}

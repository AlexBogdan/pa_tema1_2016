// Copyright 2017 Andrei Bogdan Alexandru

#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

struct Stropitoare {
 public:
	long long pos, pow;
	char direction = 0;

	Stropitoare(): pos(0), pow(0) {}
	Stropitoare(long long pos, long long pow): pos(pos), pow(pow) {}
	Stropitoare(const Stropitoare& s):
			pos(s.pos), pow(s.pow), direction(s.direction) {}
	~Stropitoare() {}
	void blockPoint() {
		this->direction = 0;
	}
	void activateLeft() {
		this->direction = -1;
	}
	void activateRight() {
		this->direction = 1;
	}
};

bool intersects(Stropitoare& left, Stropitoare& right) {
	if (left.direction <= 0 && right.direction >= 0) {
		return false;
	}
	if (left.direction <= 0 && right.direction < 0) {
		return (left.pos >= right.pos - right.pow);
	}
	if (left.direction > 0 && right.direction >= 0) {
		return (left.pos + left.pow >= right.pos);
	}
	if (left.direction > 0 && right.direction < 0) {
		return (left.pos + left.pow >= right.pos - right.pow);
	}
	return false;
}

int main() {
// -=-=-=-=-=-=-=- Declarari + Input -=-=-=-=-=-=-=-=-=
	ifstream in("stropitori.in");

	string stadiumName;
	int n, s, i;
	vector<Stropitoare> stropitori;
	Stropitoare stropitoare;
	vector<Stropitoare>::iterator strop;

	getline(in, stadiumName);
	in >> n >> s;
	for (i = 0; i < n ; ++i) {
		in >> stropitoare.pos;
		stropitori.push_back(stropitoare);
	}  // Citim pozitiile

	for (strop = stropitori.begin(); strop != stropitori.end() ; ++strop) {
		in >> strop->pow;
	}  // Citim puterile

	in.close();
// -=-=-=-=-=-=-=-=-=  Verificare stropitori  -=-=-=-=-=-=-=-=-=-=
/*
	Vom itera prin lista de stropitori (sortate) si verificam daca prin activarea
	stropitorii atingem orice alta stropitoare sau daca iesim din limite. Daca
	este ok activam stropitoarea, daca nu nu.
*/
	// Verificam daca putem activa prima stropitoare
	strop = stropitori.begin();
	if (strop->pos - strop->pow >= 0) {
		strop->activateLeft();
	} else {
		strop->activateRight();
		if (intersects(*strop, *(strop+1))) {
			strop->blockPoint();
		}
	}

	// Iteram prin stropitori si verificam vecinii din stanga si din dreapta
	for (++strop ; (strop+1) != stropitori.end() ; ++strop) {
		strop->activateLeft();
		if (intersects(*(strop-1), *strop)) {
			strop->activateRight();
			if (intersects(*strop, *(strop+1))) {
				if((strop-1)->direction == 1) {
					strop->activateLeft();
					(strop-1)->blockPoint();
					if (intersects(*(strop-1), *strop)) {
						(strop-1)->activateRight();
						strop->blockPoint();
					}
				} else {
					strop->blockPoint();
				}
			}
		}
	}

	// Verificam si ultima stropitoare
	strop->activateLeft();
	if (intersects(*(strop-1), *strop)) {
		strop->activateRight();
		if(strop-> pos + strop->pow > s) {
			strop->blockPoint();
		}
	}

	unsigned long contor = 0;  // Numaram cate storpitori sunt active
	for (strop = stropitori.begin(); strop != stropitori.end() ; ++strop) {
		if (strop->direction != 0) {
			contor++;
		}
	}

	FILE *out = fopen("stropitori.out", "w+");
	fprintf(out, "%lu", contor);
	fclose(out);

	return 0;
}

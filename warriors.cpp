// Copyright 2017 Andrei Bogdan Alexandru

#include <climits>
#include <cstdio>
#include <fstream>
#include <iostream>

using namespace std;
/* Maxim dintre 2 numere */
unsigned long getMax(unsigned long a, unsigned long b) {
    if (a >= b) {
        return a;
    } else {
        return b;
    }
}

/* 
  Cautam 
*/
unsigned long findLevel(unsigned long attempts, unsigned long maxLevel) {
    if (maxLevel == 0 || maxLevel == 1 || attempts == 1) {
        return maxLevel;
    }  // Cazuri de baza

    unsigned long min = ULONG_MAX, level, result;

    for (level = 1; level <= maxLevel; ++level) {
        result = getMax(findLevel(attempts-1, level-1),
                        findLevel(attempts, maxLevel-level));
        if (result < min) {
            min = result;
        }
    }

    return min + 1;
}

int main() {
    ifstream in("warriors.in");
    unsigned long maxLevel, attempts;

    in >> maxLevel >> attempts;
    in.close();

    FILE *out;
    out = fopen("warriors.out", "w+");
    fprintf(out, "%lu", findLevel(attempts, maxLevel));
    fclose(out);

    return 0;
}

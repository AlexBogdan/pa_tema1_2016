// Copyright 2017 Andrei Bogdan Alexandru

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

struct Interval {
 public:
	int x, y;
	Interval(): x(0), y(0) {}
	Interval(int x, int y): x(x), y(y) {}
	Interval(const Interval& i): x(i.x), y(i.y) {}
	~Interval() {}

	bool isIncluded(const Interval& i) {
		return (i.x <= this->x && this->y <= i.y);
	}

	bool intersects(const Interval& i) {
		return (i.x < this->y);
	}

	bool resize(const Interval& i) {
		this->y = i.x-1;
	}

	friend ifstream& operator >> (ifstream& in, Interval& interval);
	friend ofstream& operator << (ofstream& out, Interval& interval);
};

// Pentru citirea din fisier a intervalelor
ifstream& operator >> (ifstream& in, Interval& interval) {
    in >> interval.x >> interval.y;
    return in;
}

// Doar pentru verificari | afisam pe ecran intervalul
ofstream& operator << (ofstream& out, Interval& interval) {
    out << interval.x << " " << interval.y;
    return out;
}

// Comparam dupa inceputul intervalului
bool comp(Interval& i1, Interval& i2) {
	return (((i1.x == i2.x) && (i1.y >= i2.y)) || (i1.x <= i2.x));
}

int main() {
// -=-=-=-=-=-=-=- Declarari + Input -=-=-=-=-=-=-=-=-=

	ifstream in("points.in");
	ofstream out("points.out");

	vector<int> points;
	vector<Interval> rawIntervals;
	int n, m;
	int p;
	Interval i;
	in >> n >> m;

	if (m == 0 || m == 1) {  // Exceptiile pe care le tratam direct
		out << m;
		out.close();
		in.close();
		return 0;
	}
	for (int i = 0 ; i < n ; ++i) {
		in >> p;
		points.push_back(p);
	}
	for (int j = 0 ; j < m ; ++j) {
		in >> i;
		rawIntervals.push_back(i);
	}
	in.close();

// -=-=-=-=-=-=-=-=- Prelucrare intervale -=-=-=-=-=-=-=-=-=-=
/* 	
	Vom scoate intervalele care sunt inlcuse in alte intervale.
	Pentru intervalele ramase vom alege mereu intervalul care
	cuprinde actualul punct si care are capatul de terminare
	cat mai mare. Dupa ce numaram acest interval vom parcurge
	punctele pana cand intalnim un nou interval in care putem
	pune actualul punct si repetam procesul.
*/

	vector<Interval>::iterator fix, mobil;
	sort(rawIntervals.begin(), rawIntervals.end(), comp);

	vector<Interval> finalIntervals;
	mobil = fix = rawIntervals.begin();
	finalIntervals.push_back(*fix);
	// Eliminam intervalele incluse in alte intervale
	for (mobil++; mobil != rawIntervals.end(); ++mobil) {
		if (!mobil->isIncluded(*fix)) {
			finalIntervals.push_back(*mobil);
			fix = mobil;
		}
	}

	if (finalIntervals.size() == 1) {
		out << 1;  // Tratam cazul banal
		out.close();
		return 0;
	}

	vector<int>::iterator point;
	vector<Interval>::iterator interval;

	int noIntervals = 0;
	bool found;
	point = points.begin();
	interval = finalIntervals.begin();
	while (point != points.end() && interval != finalIntervals.end()) {
		found = false;
		while (interval != finalIntervals.end() && *point > interval->x) {
			++interval;
			found = true;
		}
		if (interval == finalIntervals.end()) break;
		if (*point == interval->x) found = true;
		while ((interval+1) != finalIntervals.end() &&
			*point >= (interval+1)->x) {
			++interval;
		}
		if (found == true) {
			noIntervals ++;
			if (interval == finalIntervals.end()) break;
			while (point != points.end() && *point < interval->y) {
				++point;
			}
			if (point == points.end()) break;
		} else {
			point++;
		}
	}
	noIntervals ++;  // Intervalul care nu se numara
	out << noIntervals;
	out.close();
	return 0;
}
